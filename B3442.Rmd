---
title: "B3442"
author: "Richelmy Mathieu et Zhou Thomas"
date: "15/05/2019"
output: html_document
---

# Compte-Rendu TP Generation de nombres aleatoires et probabilites

## 1. Tests de generateurs pseudo-aleatoires
  **+ Generateurs**  
  Voici les deux fonctions realisees : 
  ```{r eval = FALSE}
  #Paramètres des fonctions
a <- 65539
b <- 0
k = 5
# k taille de la sequence
graine = sample.int(10000,1)

RANDU <- function(a, b, k, s0)
{
  m = 2^31
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

StandardMinimal <- function(a, b, k, s0)
{
  m = (2^31)-1
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

#Test des fonctions
RANDU(a, b, k,graine)
StandardMinimal(16807, b, k,graine)
```
  **+ Tests visuels**  
  On travaillera sur une sequence de k=1000 valeurs.  
  *1) Histogrammes des sorties observees pour les 4 generateurs etudies : *
  ```{r echo = FALSE}
source('generateurs.R')

a <- 65539
b <- 0
k = 1000

RANDU <- function(a, b, k, s0)
{
  m = 2^31
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

StandardMinimal <- function(a, b, k, s0)
{
  m = (2^31)-1
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

graine = sample.int(10000,1)  
resSM = StandardMinimal(16807, b, k, graine)
resRANDU = RANDU(a, b, k, graine)
resVN = VonNeumann(k,1,graine)
resMT = MersenneTwister(k,1,graine)

par(mfrow=c(2,2))
hist(resSM)
hist(resRANDU)
hist(resVN)
hist(resMT)  
  ```  
    
On peut remarquer qu'entre les deux histogrammes de repartition de nos fonctions, RANDU et SM, il n'y a quasiment aucune difference. On semble bien suivre une loi uniforme, avec quelques fluctuations dû à l'aleatoire. Il n'y a pas de difference notable avec MersenneTwister mais bien avec Von Neumann, qui ne semble pas du tout suivre une loi uniforme.  
Dans le second test visuel, on va chercher à observer la repartition en fonction de la valeur precedente :  
  ```{r echo = FALSE}
source('generateurs.R')

a <- 65539
b <- 0
k = 1000

RANDU <- function(a, b, k, s0)
{
  m = 2^31
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

StandardMinimal <- function(a, b, k, s0)
{
  m = (2^31)-1
  res = vector("double", k)
  res[1] = s0
  for (i in 2:k){
    res[i] <- (a*res[i-1] + b)%%m
  }
  return(res)
}

graine = sample.int(10000,1)  
resSM = StandardMinimal(16807, b, k, graine)
resRANDU = RANDU(a, b, k, graine)
resVN = VonNeumann(k,1,graine)
resMT = MersenneTwister(k,1,graine)

par(mfrow=c(2,2))
plot(resRANDU[1:k-1], resRANDU[2:k])
plot(resSM[1:k-1], resSM[2:k])
plot(resVN[1:k-1], resVN[2:k])
plot(resMT[1:k-1], resMT[2:k])
```  
  
  Comme observe precedement, il n'y a pas de difference pertinente entre les resultats obtenus par les generateurs Standard Minimal, RANDU et MerenneTwister. Seul se demarque celui de Von Neumann, car il ne suit pas une loi uniforme.  
  **+ Tests de frequence monobit**  
A partir de ce test et pour les prochains, nous utiliserons une sequence de k=1000 valeurs et pour 100 initialisations differentes.  
Voici la fonction Frequency :  
```{r eval = FALSE}
#x le vecteur des nombres observes
#nb le nb de bits à considerer
Frequency <- function (x, nb)
{
  sn = 0
  for (i in 1:length(x)){
    binaire = binary(x[i])
    for (j in (33-nb):32){
      binaire[j] = 2*binaire[j]-1
      sn = sn + binaire[j]
    }
  }
  sobs = abs(sn) / sqrt(nb*length(x))
  pvaleur = 2 * (1 - pnorm(sobs))
  return(pvaleur)
}
```  
   
On obtient les repartitions ci-dessous, en fonction des differents generateurs:  
```{r echo = FALSE}
Frequency <- function (x, nb)
{
  sn = 0
  for (i in 1:length(x)){
    binaire = binary(x[i])
    for (j in (33-nb):32){
      binaire[j] = 2*binaire[j]-1
      sn = sn + binaire[j]
    }
  }
  sobs = abs(sn) / sqrt(nb*length(x))
  pvaleur = 2 * (1 - pnorm(sobs))
  return(pvaleur)
}

ftestSM = vector('double', 100)
ftestMT = vector('double', 100)
ftestRandu = vector('double', 100)
ftestVN = vector('double', 100)
for (i in 1:100){
  graine = sample.int(10000,1)  
  resSM = StandardMinimal(16807, b, k, graine)
  resRANDU = RANDU(a, b, k, graine)
  resVN = VonNeumann(k,1,graine)
  resMT = MersenneTwister(k,1,graine)
  ftestRandu[i] = Frequency(resRANDU,31)
  ftestSM[i] = Frequency(resSM,31)
  ftestVN[i] = Frequency(resVN,13)
  ftestMT[i] = Frequency(resMT,32)
}

#resultats
par(mfrow=c(2,2))
plot(1:100,ftestSM)
plot(1:100,ftestVN)
plot(1:100,ftestMT)
plot(1:100,ftestRandu)
```  
  
On peut en conclure que le generateur de Von Neumann n'est pas valide (tous les resulats trouves sont inferieurs à 1%). Les generateur Standard Minimal et MereyenneTwister ne sont pas invalide par ce test, et celui base sur RANDU depend de la graine, bien qu'il soit en majorite invalide.  
  
  **+ Tests des runs**  
Ci-dessous la fonction Runs :
```{r eval = FALSE}
Runs <- function(x, nb){
  vn = 1
  pi = 0
  nbBitsTotal <- nb * length(x)
  tau <- 2 / sqrt(nbBitsTotal)
  lastBit <- 0
  nbDeUns <- 0
  for (i in 1:length(x)){
    binaire = binary(x[i])
    for (j in (33-nb):32){
      # pr?-test
      if (binaire[j] == 1) {
        nbDeUns = nbDeUns + 1;
      }
    }
  }
  pi <- nbDeUns / nbBitsTotal
  if (abs(pi-(1/2)) >= tau) {
    return(0)
  } else {  #test
    for (i in 1:length(x)){
      binaire = binary(x[i])
      for (j in (33-nb):32){
        if (binaire[j] != binaire[j+1] && j!=32) {
          vn = vn + 1
        }
        lastBit = binaire[j]
        if ( (j == 32) && (i!=length(x)) && (lastBit != binary(x[i+1])[1]) ) { 
          vn = vn + 1
        }
      }
    }
  }
  Pvaleur = 2 * (1 - pnorm((abs(vn - 2*nbBitsTotal*pi*(1-pi) ))/(2*sqrt(nbBitsTotal)*pi*(1-pi))))
  return(Pvaleur)
}
```  
Et les resultats obtenus pour les differents generateurs a ce test :
```{r echo = FALSE}
Runs <- function(x, nb){
  vn = 1
  pi = 0
  nbBitsTotal <- nb * length(x)
  tau <- 2 / sqrt(nbBitsTotal)
  lastBit <- 0
  nbDeUns <- 0
  for (i in 1:length(x)){
    binaire = binary(x[i])
    for (j in (33-nb):32){
      # pr?-test
      if (binaire[j] == 1) {
        nbDeUns = nbDeUns + 1;
      }
    }
  }
  pi <- nbDeUns / nbBitsTotal
  if (abs(pi-(1/2)) >= tau) {
    return(0)
  } else {  #test
    for (i in 1:length(x)){
      binaire = binary(x[i])
      for (j in (33-nb):32){
        if (binaire[j] != binaire[j+1] && j!=32) {
          vn = vn + 1
        }
        lastBit = binaire[j]
        if ( (j == 32) && (i!=length(x)) && (lastBit != binary(x[i+1])[1]) ) { 
          vn = vn + 1
        }
      }
    }
  }
  Pvaleur = 2 * (1 - pnorm((abs(vn - 2*nbBitsTotal*pi*(1-pi) ))/(2*sqrt(nbBitsTotal)*pi*(1-pi))))
  return(Pvaleur)
}

runsSM = vector('double', 100)
runsMT = vector('double', 100)
runsRANDU = vector('double', 100)
runsVN = vector('double', 100)
for (i in 1:100){
  graine = sample.int(10000,1)  
  resSM = StandardMinimal(16807, b, k, graine)
  resRANDU = RANDU(a, b, k, graine)
  resVN = VonNeumann(k,1,graine)
  resMT = MersenneTwister(k,1,graine)
  
  runsSM[i] =Runs(resSM, 31)
  runsRANDU[i] = Runs(resRANDU, 31)
  runsMT[i] = Runs(resMT,32)
  runsVN[i] = Runs(resVN,13)
}

par(mfrow=c(2,2))
plot(1:100,runsMT)
plot(1:100,runsRANDU)
plot(1:100,runsSM)
plot(1:100,runsVN)
```  
  
  On remarque que le test permet d'invalider les generateur RANDU et de Von Neumann, puisque leurs résultats sont inférieurs à 1%. Cela confirme les observations precedentes. De meme, les generateurs Standard Minimal et Mereynne-Twister semble bien suivre une loi uniforme.  
  *Commentaire : *Sur ce test, il nous est arrivé de voir apparaitre des motifs, dont nous n'avons pas trouvé la raison...  
    
  **+ Tests d'ordre**  
  ``` {r echo=FALSE}
  library(randtoolbox)
  ordreSM = vector('double', 100)
  ordreMT = vector('double', 100)
  ordreRANDU = vector('double', 100)
  ordreVN = vector('double', 100)
  for (i in 1:100){
    graine = sample.int(10000,1)  
    resSM = StandardMinimal(16807, b, k, graine)
    resRANDU = RANDU(a, b, k, graine)
    resVN = VonNeumann(k,1,graine)
    resMT = MersenneTwister(k,1,graine)
    
    ordreSM[i] = order.test(resSM, d=4, echo=FALSE)$p.value
    ordreRANDU[i] = order.test(resVN[,1], d=4, echo=FALSE)$p.value
    ordreMT[i] = order.test(resMT[,1], d=4, echo=FALSE)$p.value
    ordreVN[i] = order.test(resRANDU, d=4, echo=FALSE)$p.value
  }
  par(mfrow=c(2,2))
  plot(1:100,ordreMT)
  plot(1:100,ordreRANDU)
  plot(1:100,ordreSM)
  plot(1:100,ordreVN)
  ```
  
Ce test permet d'invalider le generateur RANDUU mais pas les trois autres. Il nous permet de montrer qu'il ne faut pas se baser que sur un seul type de test, puisque celui-ci n'invalide pas le generateur de Von Neumann, contrairement aux autres tests.  
*Commentaire : *Ce test aussi faisait apparaitre des motifs, pour le meme resultat que le precedent.  

## 2. Application aux files d'attentes  

Ci-dessous la fonction FileMM1, qui à partir des paramètres des lois exponentielles et de la duree d'observation retourne les dates d'arrivees et de departs des clients du système :  
```{r eval = FALSE}
FileMM1 <- function(lambda, mu, D){
  #initialisation
   arrivee <- vector()
   depart <- vector()

  #tableau arrivee
  i <- 1
  arrivee[i] <- rexp(1,lambda)
  current <- arrivee[i] + rexp(1,lambda)
  while ( current < D) {
    arrivee[i+1] = current
    current <- arrivee[i+1] + rexp(1,lambda)
    i = i + 1
  }
  
  #tableau depart
  depart[1] <- arrivee[1] + rexp(1,mu)
  time = depart[1]
  # si y a des clients arrives
  if(length(arrivee) > 1){
    i <- 1
    while( time < D && (i < length(arrivee)) ){
      if (depart[i] < arrivee[i+1]){
        time = arrivee[i+1] + rexp(1,mu)
      } else {
        time = depart[i] + rexp(1,mu)
      }
      if (time < D) {
        depart[i+1] = time
        i <- i+1
      }
    }
  }
  res <- list(arrivee,depart) 
  return (res)
}
```  
Cette fonction va nous servir à visualiser l'evolution d'une file d'attente, en faisant varier les paramètres pour analyser leurs influences.  
Soit la fonction EvolNbClients qui prend en entrees les dates d'arrivees et de sorties, retourne l'evolution du nombre de client en fonction du temps:  
```{r eval = FALSE}
evolNbClients <- function(arrivee, depart){
  mat <- matrix(ncol=0, nrow=2)
  nbClients <- 0
  j <- 1
  for (i in 1:length(arrivee)) {
    time <- arrivee[i]
    #Pour chaque depart se deroulant avant l'arrivee du client, on l'ajoute au resultat   
    while (depart[j] <  time){
      nbClients <- nbClients - 1
      mat = cbind(mat, c(depart[j],nbClients))
      j = j + 1
    }
    nbClients <- nbClients + 1
    mat = cbind(mat, c(time,nbClients))
  }
  return(mat)
}
```  
Application : On souhaite comparer les resultats obtenus avec 8, 14, 15 et 20 arrivees en moyenne par heure, pour 15 departs par heure. Ainsi on a : $\lambda = x/60$ et $\mu = 15/60$ (avec $x={8,14,15,20}$) On travaillera sur 12H de fonctionnement (D=12 * 60) : 
```{r echo = FALSE}
# Q6
#Fonction FileMM1
#Lambda : Parametre de la loi exponentielle des arrivees
#mu : parametre de la loi exponentielle des departs
#D : temps d'observation du système
FileMM1 <- function(lambda, mu, D){
  #initialisation
   arrivee <- vector()
   depart <- vector()
   
  #tableau arrivee
  i <- 1
  arrivee[i] <- rexp(1,lambda)
  current <- arrivee[i] + rexp(1,lambda)
  while ( current < D) {
    arrivee[i+1] = current
    current <- arrivee[i+1] + rexp(1,lambda)
    i = i + 1
  }
  
  #tableau depart
  depart[1] <- arrivee[1] + rexp(1,mu)
  time = depart[1]
  # s'il y a des clients arrives :
  if(length(arrivee) > 1){
    i <- 1
    while( time < D && (i < length(arrivee)) ){
      if (depart[i] < arrivee[i+1]){
        time = arrivee[i+1] + rexp(1,mu)
      } else {
        time = depart[i] + rexp(1,mu)
      }
      if (time < D) {
        depart[i+1] = time
        i <- i+1
      }
    }
  }
  res <- list(arrivee,depart) 
  return (res)
}

# Q7
lambda <- 0.2
mu <- 1.5
D<- 300
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]

evolNbClients <- function(arrivee, depart){
  mat <- matrix(ncol=0, nrow=2)
  nbClients <- 0
  j <- 1
  for (i in 1:length(arrivee)) {
    time <- arrivee[i]
    #Pour chaque depart se deroulant avant l'arrivee du client, on l'ajoute au resultat   
    while ((depart[j] <  time) && (j < length(depart)) ){
      nbClients <- nbClients - 1
      mat = cbind(mat, c(depart[j],nbClients))
      j = j + 1
    }
    nbClients <- nbClients + 1
    mat = cbind(mat, c(time,nbClients))
  }
  return(mat)
}

#Application
# 8/60 = lambda pour avoir 8 clients qui arrivent en moyenne par heure
# 15/60 = mu pour avoir 15 clients qui partent en moyenne par heure
lambda <- (8/60)
mu <- (15/60)
D<- (12*60)
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]
res8 = evolNbClients(dateArrivee, dateSortie)
lambda <- (14/60)
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]
res14 = evolNbClients(dateArrivee, dateSortie)
lambda <- (15/60)
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]
res15 = evolNbClients(dateArrivee, dateSortie)
lambda <- (20/60)
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]
res20 = evolNbClients(dateArrivee, dateSortie)

par(mfrow=c(2,2))
plot(t(res8))
plot(t(res14))
plot(t(res15))
plot(t(res20))
```  
  
  On remarque que pour 8 clients par heure, on a que peu de variations, avec des pics bas (on tend vers un régime stationnaire). Par contre, quand \lambda et \mu s'approche (pour les cas de 14 et 15), on ne peut rien prévoir.Pour finir, quand on a plus d'arrivées que de départs, on va suivre une courbe linéaire croissante, menant à la saturation du système.  
  Le nombre moyen de clients calculé sur 12H d'observation en reprenant les conditions de régime stationnaire, est `r mean(res8[,2])`. 
  On calcule le temps de presence d'un client Tmoy (en minutes):
  ```{r echo = FALSE} 
i <- 1
lambda <- 8/60 #on revient à lambda = 8, car régime stationnaire
res = FileMM1(lambda,mu,D)
dateArrivee = res[[1]]
dateSortie = res[[2]]
duree <- vector()
while ( (i <= length(dateSortie)) ){
      duree <- append(duree, dateSortie[i] - dateArrivee[i])
      i <- i + 1
}
mean(duree) #7.19
```
  
On calcule E(N), $\alpha/(\alpha-1) =$ `r (8/15)/(1-(8/15))`. En le comparant à $\lambda * Tmoy =$ `r mean(duree)*lambda`, on remarque qu'ils sont proches, donc on retrouve bien la formule de Little, ce qui confirme le régime stationnaire observé.  
  
  